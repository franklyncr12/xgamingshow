from django.db import models

# Create your models here.


class Actividad(models.Model):
    title = models.CharField(max_length=200, verbose_name="Titulo")
    content = models.TextField(verbose_name="Contenido")
    image = models.ImageField(verbose_name="imagen", upload_to="actividades")
    created = models.DateTimeField(
        auto_now_add=True, verbose_name="Fecha de Creación")
    updated = models.DateTimeField(
        auto_now=True, verbose_name="Fecha de Edición")

    class Meta:
        verbose_name = "actividad"
        verbose_name_plural = "actividades"
        ordering = ["-created"]

    def __str__(self):
        return self.title


class Patrocinador(models.Model):
    name = models.CharField(max_length=200, verbose_name="Nombre")
    business_activity = models.TextField(verbose_name="Actividad de negocios")
    image = models.ImageField(verbose_name="logo", upload_to="patrocinadores")
    created = models.DateTimeField(
        auto_now_add=True, verbose_name="Fecha de Creación")
    updated = models.DateTimeField(
        auto_now=True, verbose_name="Fecha de Edición")

    class Meta:
        verbose_name = "patrocinador"
        verbose_name_plural = "patrocinadores"
        ordering = ["-created"]

    def __str__(self):
        return self.name


class Entrada(models.Model):
    title = models.CharField(max_length=200, verbose_name="Título")
    price = models.FloatField(verbose_name="Precio")
    created = models.DateTimeField(
        auto_now_add=True, verbose_name="Fecha de Creación")
    updated = models.DateTimeField(
        auto_now=True, verbose_name="Fecha de Edición")

    class Meta:
        verbose_name = "entrada"
        verbose_name_plural = "entradas"
        ordering = ["-created"]

    def __str__(self):
        return self.title


class Conferencista(models.Model):
    name = models.CharField(max_length=200, verbose_name="Nombres")
    lastname = models.CharField(max_length=200, verbose_name="Apellidos")
    company = models.CharField(max_length=200, verbose_name="Empresa")
    position = models.CharField(max_length=200, verbose_name="Cargo")
    topic = models.CharField(max_length=200, verbose_name="Tema")
    cv = models.TextField(verbose_name="Hoja de Vida")
    image = models.ImageField(
        verbose_name="foto", upload_to="conferencistas", default="null")
    created = models.DateTimeField(
        auto_now_add=True, verbose_name="Fecha de Creación")
    updated = models.DateTimeField(
        auto_now=True, verbose_name="Fecha de Edición")

    class Meta:
        verbose_name = "conferencista"
        verbose_name_plural = "conferencistas"
        ordering = ["-created"]

    def __str__(self):
        return self.name
