from django.shortcuts import render, HttpResponse
from .models import Actividad, Patrocinador, Entrada, Conferencista
# Create your views here.


def home(request):
    actividades = Actividad.objects.all()
    patrocinadores = Patrocinador.objects.all()
    entradas = Entrada.objects.all()
    conferencistas = Conferencista.objects.all()
    return render(request, "core/index.html", {"actividades": actividades, "patrocinadores": patrocinadores, "entradas": entradas, "conferencistas": conferencistas})
