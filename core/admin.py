from django.contrib import admin
from .models import Actividad, Patrocinador, Entrada, Conferencista

# Register your models here.


class ActividadAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')


class PatrocinadorAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')


class EntradaAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')


class ConferencistaAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')


admin.site.register(Actividad, ActividadAdmin)
admin.site.register(Patrocinador, PatrocinadorAdmin)
admin.site.register(Entrada, EntradaAdmin)
admin.site.register(Conferencista, ConferencistaAdmin)
